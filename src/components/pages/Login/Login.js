import React from 'react'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'

import {submitUserLogin, submitNewUser, updateLoginForm} from '../../state/actions/actions'

class Login extends React.Component {

    //define React handlers

    constructor(props) {
        super(props);
    }

    componentWillUnmount() {
        this.props.dispatch(updateLoginForm({}));
    }
    
    //define and implement functions and handlers

    errorMessage = () => {
        if (this.props.error) {
            return (<h3 className='error-message'>{this.props.message}</h3>)
        } else {
            return null;
        }
    };
    
    handleLoginEvent = (e) => {
        e.preventDefault();

        let user = {
            username: this.props.loginForm.loginUsername,
            password: this.props.loginForm.loginPassword
        };

        this.props.dispatch(submitUserLogin(user, this.props.dispatch));
    };

    handleRegisterEvent = (e) => {
        e.preventDefault();

        let newUser = {
            username: this.props.loginForm.registrationUsername,
            password: this.props.loginForm.registrationPassword,
            email: this.props.loginForm.registrationEmail,
        };

        if (this.props.loginForm.registrationPassword === this.props.loginForm.registrationMatchPassword) {
            this.props.dispatch(updateLoginForm({...this.props.loginForm, showPasswordMismatchMessage: false}));
            this.props.dispatch(submitNewUser(newUser, this.props.dispatch));
        } else {
            this.props.dispatch(updateLoginForm({...this.props.loginForm, showPasswordMismatchMessage: true}));
        }
    };

    handleLoginUsernameChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, loginUsername: event.target.value}));
    };

    handleLoginPasswordChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, loginPassword: event.target.value}));
    };

    handleRegistrationUsernameChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, registrationUsername: event.target.value}));
    };

    handleRegistrationPasswordChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, registrationPassword: event.target.value}));
    };

    handlePasswordCheckChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, registrationMatchPassword: event.target.value}));
    };

    handleRegistrationEmailChange = (event) => {
        this.props.dispatch(updateLoginForm({...this.props.loginForm, registrationEmail: event.target.value}));
    };


    render() {
        //define the component
        let component = (<section className="welcome_area login clearfix d-flex">
            <div className="login-wrap ">
                <div className="login-html">
                    <input id="tab-1" type="radio" name="tab" className="sign-in" defaultChecked/>
                    <label htmlFor="tab-1" className="tab">SignIn</label>
                    <input id="tab-2" type="radio" name="tab" className="sign-up"/>
                    <label htmlFor="tab-2" className="tab">SignUp</label>
                    <div className="login-form">
                        <form onSubmit={(event) => {
                            this.handleLoginEvent(event)
                        }}>
                            <div className="sign-in-htm">
                                <div className="group">
                                    <label htmlFor="user" className="label">Username</label>
                                    <input id="loginUsername" type="text" className="input"
                                           onChange={(event) => {
                                               this.handleLoginUsernameChange(event)
                                           }}/>
                                </div>
                                <div className="group">
                                    <label htmlFor="pass" className="label">Password</label>
                                    <input id="loginPassword" type="password" className="input" data-type="password"
                                           onChange={(event) => {
                                               this.handleLoginPasswordChange(event)
                                           }}/>
                                </div>
                                <div className="group">
                                    <input type="submit" className="button" value="Sign In"/>
                                    <br/>
                                    {this.errorMessage()}
                                </div>
                            </div>
                        </form>
                        <form onSubmit={(event) => {
                            this.handleRegisterEvent(event)
                        }}>
                            <div className="sign-up-htm">
                                <div className="group">
                                    <label htmlFor="registrationUsername" className="label">Username</label>
                                    <input id="registrationUsername" type="text" className="input"
                                           onChange={(event) => {
                                               this.handleRegistrationUsernameChange(event)
                                           }}/>
                                </div>
                                <div className="group">
                                    <label htmlFor="registrationPassword" className="label">Password</label>
                                    <input id="registrationPassword" type="password" className="input"
                                           data-type="password" onChange={(event) => {
                                        this.handleRegistrationPasswordChange(event)
                                    }}/>
                                </div>
                                <div className="group">
                                    <label htmlFor="registrationRetypePassword" className="label">Repeat
                                        Password</label>
                                    <input id="registrationRetypePassword" type="password" className="input"
                                           data-type="password" onChange={(event) => {
                                        this.handlePasswordCheckChange(event)
                                    }}/>
                                </div>
                                <div className="group">
                                    <label htmlFor="registrationEmail" className="label">Email Address</label>
                                    <input id="registrationEmail" type="text" className="input"
                                           onChange={(event) => {
                                               this.handleRegistrationEmailChange(event)
                                           }}/>
                                </div>
                                <div className="group">
                                    <input type="submit" className="button" value="Sign Up"/>
                                    <br/>
                                    {this.props.loginForm.showPasswordMismatchMessage ?
                                        <h3 className={'error-message'}> Password not match.</h3> : null}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>);
        
        //decide what to render
        if (this.props.status === "success") {
            this.props.dispatch(updateLoginForm({...this.props.loginForm, showPasswordMismatchMessage: false}));
            return (<Redirect to={'/userProfile'}/>);
        } else
            return component;
    };
}


const mapStateToProps = state => {
    return {
        isSubmitting: state.userReducer.isSubmitting,
        status: state.userReducer.response.status,
        message: state.userReducer.response.message,
        error: state.userReducer.response.error,
        loginForm: state.userReducer.loginForm
    }
};


export default connect(mapStateToProps)(Login)