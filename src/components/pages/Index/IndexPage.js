import React from 'react'
import logo from '../../../resources/img/x-men-logo.png'

const IndexPage = () => (
    <section className="welcome_area clearfix" id="home">
        <div className="hero-slides">
            <div className="single-hero-slide d-flex align-items-start justify-content-center home-page-wrapper">
                <div className="hero-slide-content text-center">
                    <h2>Retro Storm App</h2>
                    <h4>Developed by X-Men</h4>
                    <img style={{marginTop: "100px"}} src={logo} alt="x-men"/>
                </div>
            </div>

        </div>
    </section>
);

export default IndexPage