import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../state/actions/actions'
import { bindActionCreators } from 'redux'

class CreateRetroPage extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.actions.getRetroItems("id");
    }

    handleNewRetroSubmit(e) {
        this.props.actions.createNewRetro();
        this.props.history.push("/retroRoom");
    }

    handleChangeRetroName(e) {

    }

    errorMessage(e) {

    }

    render() {
        let {message, retroItems} = this.props;

        return (
            <section className="user-page-wrapper clearfix">
                <div className="single-hero-slide d-flex justify-content-center">
                    <div className="hero-slide-content">
                        <div>
                            <h2>Create new Retro</h2>
                            <div className={"newRetroWrapper"}>
                                    <div className="new-retro-wrap ">
                                        <div className="login-html">
                                            <input id="tab-1" type="radio" name="tab" className="sign-in" defaultChecked/>
                                            <label id="submitRetroLabel" htmlFor="tab-1" className="tab">Submit Retro</label>
                                            <div className="login-form">
                                                <form onSubmit={(event) => {
                                                    this.handleNewRetroSubmit(event)
                                                }}>
                                                    <div className="submit-retro-wrapper">
                                                        <div className="group">
                                                            <label htmlFor="user" className="label">RetroName</label>
                                                            <input id="retroName" type="text" className="input"
                                                                   onChange={(event) => {
                                                                       this.handleChangeRetroName(event)
                                                                   }}/>
                                                        </div>
                                                        <div className="group">
                                                            <input type="submit" className="button" value="Create Retro"/>
                                                            <br/>
                                                            {this.errorMessage()}
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        );

    }

}

const mapStateToProps = (state) => {
    return {
        message: state.userReducer.response.message,
        retroItems: state.retroItemReducer.response.retroItems
    }
};

function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(actionCreators, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateRetroPage)