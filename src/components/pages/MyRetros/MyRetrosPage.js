import React from 'react'
import {connect} from 'react-redux'
import RetroItem from './RetroItem'
import * as actionCreators from '../../state/actions/actions'
import {bindActionCreators} from 'redux'
import Popup from 'reactjs-popup'
import RetroItemModal from "./RetroItemModal"

class MyRetrosPage extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.actions.getRetroItems("id");
    }

    handleNewRetroClick = () => {
        this.props.history.push('/createRetro')
    }

    renderRetroItems = (retroItems) => {
        let vars = [];

        if (retroItems !== undefined && retroItems instanceof Array) {
            retroItems.map(retroItem => {
                return vars.push(
                    <Popup key={retroItem.id} trigger={<div id="retroItemWrapper" className={'retro-item col-12 col-sm-3 col-lg-3'}>
                        <RetroItem retroItem={retroItem}/></div>} modal closeOnDocumentClick>
                        <div id="popup" className={"retroDetailsModal"}>
                            <RetroItemModal retroItem={retroItem}/>
                        </div>
                    </Popup>
                )
            })
        }

        return vars;

    };

    render() {
        let {message, retroItems} = this.props;

        return (
            <section className="user-page-wrapper clearfix">
                <div className="single-hero-slide d-flex justify-content-center">
                    <div className="hero-slide-content">
                        <div>
                            <h2>My retros </h2>
                            <button type="button" className={"retroButton"} onClick={e => {
                                e.preventDefault();
                                this.handleNewRetroClick()
                            }}> start new retro
                            </button>
                        </div>
                        <div className={"row retro-items-scroll-pane"}>
                            {this.renderRetroItems(retroItems)}
                        </div>
                    </div>
                </div>
            </section>
        );

    }

}

const mapStateToProps = (state) => {
    return {
        message: state.userReducer.response.message,
        retroItems: state.retroItemReducer.response.retroItems
    }
};

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators(actionCreators, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(MyRetrosPage)