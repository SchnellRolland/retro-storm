import React from "react"
import {Fragment} from 'react'
import {connect} from 'react-redux'
import {renderImage, renderMorale} from "../../../services/RetroItemService";

const RetroItem = ({retroItem}) => (
    <Fragment>

            <img src={renderImage(retroItem.watsonValue)} alt="img here"/>
            <div className={"row"}>
                <p id="date">{retroItem.date}</p>
                <p id="morale">Morale: <span className={renderMorale(retroItem.morale)}> {retroItem.morale} </span></p>
            </div>


    </Fragment>
);


export default connect()(RetroItem)