import React from "react"
import {Fragment} from 'react'
import {renderImage, renderMorale} from "../../../services/RetroItemService";

const RetroItemModal = ({retroItem}) => (
    <Fragment>
        <div className={"row"}>
            <p id="modalDate">Date: <span>{retroItem.date}</span></p>
            <p id="modalMorale">Morale: <span className={renderMorale(retroItem.morale)}> {retroItem.morale} </span></p>
        </div>
        <div className={"retroDetailsWrapper row"}>
            <div className={"col-12 col-sm-4 col-lg-4"}>
                <h3 style={{color: "#E16200"}} align="center">Stop</h3>
                <div className="start-doing-retro-detail">
                    {retroItem.retroDetails.stop.map(stopItem => {return (<span key={stopItem} className={"row"}>{stopItem}</span>)})}
                </div>
            </div>
            <div className={"col-12 col-sm-4 col-lg-4"}>
                <h3 style={{color: "#62a34e"}} align="center">Start</h3>
                <div className="start-doing-retro-detail">
                    {retroItem.retroDetails.start.map(startItem => {return (<span key={startItem} className={"row"}>{startItem}</span>)})}
                </div>
            </div>
            <div className={"col-12 col-sm-4 col-lg-4"}>
                <h3 style={{color: "#008283"}} align="center">Keep</h3>
                <div className="keep-doing-retro-detail">
                    {retroItem.retroDetails.keep.map(keepItem => {return (<span key={keepItem} className={"row"}>{keepItem}</span>)})}
                </div>
            </div>
        </div>
        <div className={"row"}>
            <img src={renderImage(retroItem.watsonValue)} alt="img here"/>
        </div>
    </Fragment>
);

export default RetroItemModal;