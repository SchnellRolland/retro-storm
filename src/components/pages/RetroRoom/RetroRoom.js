import React from 'react'
import {connect} from 'react-redux'
import * as actionCreators from '../../state/actions/actions'
import {bindActionCreators} from 'redux'
import {randomUUID} from "../../../services/RetroItemService";

class RetroRoom extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            startRetroItem: {},
            stopRetroItem: {},
            keepRetroItem: {}
        };
    }

    componentWillMount() {
    }

    handleAddItem(type, e) {
        if (type === "stop" && this.state.stopRetroItem.value !== undefined) {
            this.props.actions.addRetroRoomItem(this.state.stopRetroItem, this.props.dispatch);
        } else if (type === "start" && this.state.startRetroItem.value !== undefined) {
            this.props.actions.addRetroRoomItem(this.state.startRetroItem, this.props.dispatch);
        } else if (type === "keep" && this.state.keepRetroItem.value !== undefined) {
            this.props.actions.addRetroRoomItem(this.state.keepRetroItem, this.props.dispatch);
        }

        this.setState({startRetroItem: {}, stopRetroItem: {}, keepRetroItem:{}});

        document.getElementById("stopDoingInput").value = "";
        document.getElementById("startDoingInput").value = "";
        document.getElementById("keepDoingInput").value = "";
    }

    handleInputChange(type, e) {
        if (type === "start") {
            this.setState({startRetroItem: {id: randomUUID(), type: type, value: e.target.value}})
        } else if (type === "stop") {
            this.setState({stopRetroItem: {id: randomUUID(), type: type, value: e.target.value}})
        } else if (type === "keep") {
            this.setState({keepRetroItem: {id: randomUUID(), type: type, value: e.target.value}})
        }
    }

    submitFinishRetro(e) {

    }

    render() {
        return (
            <section className="user-page-wrapper clearfix">
                <div className="single-hero-slide d-flex justify-content-center">
                    <div className="hero-slide-content retro-room-wrapper">
                        <h2>Retro Room</h2>
                        <div className="row link-section" id="keepDoingRetroRoom">
                            <h4>Give this link to others: <span id="retroLink">LINK</span></h4>
                        </div>
                        <div className={"row"}>
                            <div className={"col-12 col-sm-4 col-lg-4"}>
                                <h3 style={{color: "#E16200"}} align="center">Stop</h3>
                                <div className="stop-doing-retro-detail" id="stopDoingRetroRoom">
                                    <input id="stopDoingInput" className={"input-field"} onChange={event => {
                                        this.handleInputChange("stop", event)
                                    }}/>
                                    <input type="button" className="retro-item-button" value="add" onClick={event => {
                                        this.handleAddItem("stop", event)
                                    }}/>
                                    <div style={{padding: "10px 0 0 5px"}}>
                                        {this.props.retroRoomItems.stop.map(item => {
                                            return (<span key={item.id} className="row"> {item.value}</span>)
                                        })}

                                    </div>
                                </div>
                            </div>
                            <div className={"col-12 col-sm-4 col-lg-4y"}>
                                <h3 style={{color: "#62a34e"}} align="center">Start</h3>
                                <div className="start-doing-retro-detail" id="startDoingRetroRoom">
                                    <input id="startDoingInput" className={"input-field"} onChange={event => {
                                        this.handleInputChange("start", event)
                                    }}/>
                                    <input type="button" className="retro-item-button" value="add" onClick={event => {
                                        this.handleAddItem("start", event)
                                    }}/>
                                    <div style={{padding: "10px 0 0 5px"}}>
                                        {this.props.retroRoomItems.start.map(item => {
                                            return (<span key={item.id} className="row"> {item.value}</span>)
                                        })}
                                    </div>
                                </div>
                            </div>
                            <div className={"col-12 col-sm-4 col-lg-4"}>
                                <h3 style={{color: "#008283"}} align="center">Keep</h3>
                                <div className="keep-doing-retro-detail" id="keepDoingRetroRoom">
                                    <input id="keepDoingInput" className={"input-field"} onChange={event => {
                                        this.handleInputChange("keep", event)
                                    }}/>
                                    <input type="button" className="retro-item-button" value="add" onClick={event => {
                                        this.handleAddItem("keep", event)
                                    }}/>
                                    <div style={{padding: "10px 0 0 5px"}}>
                                        {this.props.retroRoomItems.keep.map(item => {
                                            return (<span key={item.id} className="row"> {item.value}</span>)
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row retro-room-bottom-wrapper">
                            <input type="button" value="Finish Retro" onClick={event => {this.submitFinishRetro(event)}}/>
                        </div>
                    </div>
                </div>
            </section>
        );

    }

}

const mapStateToProps = (state) => {
    return {
        message: state.userReducer.response.message,
        retroRoomItems: state.retroItemReducer.retroRoomItems
    }
};

function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators(actionCreators, dispatch), dispatch: dispatch}
}

export default connect(mapStateToProps, mapDispatchToProps)(RetroRoom)