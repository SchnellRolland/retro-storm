import React, {Fragment} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';


import Header from '../Header/Header';
import IndexPage from '../pages/Index/IndexPage';
import Login from '../pages/Login/Login';
import Footer from '../Footer/Footer'
import MyRetrosPage from "../pages/MyRetros/MyRetrosPage";
import CreateRetroPage from "../pages/CreateRetroPage/CreateRetroPage";
import RetroRoom from "../pages/RetroRoom/RetroRoom"



const AppRouter = () => (
    <BrowserRouter>
        <Fragment>
            <Header/>
            <div className="container">
                <Switch>
                    <Route path='/' component={IndexPage} exact={true}/>
                    <Route path='/login' component={Login}/>
                    <Route path='/myRetros' component={MyRetrosPage} />
                    <Route path='/createRetro' component={CreateRetroPage}/>
                    <Route path='/retroRoom' component={RetroRoom}/>
                    <Redirect to="/"/>
                </Switch>
            </div>
            <Footer />
        </Fragment>
    </BrowserRouter>);

export default AppRouter;