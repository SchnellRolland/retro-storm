import React from "react"
import { NavLink } from 'react-router-dom';

import logo from '../../resources/img/logo.png';

const Footer = () => (
    <footer className="footer-area clearfix">
        <div className="top-footer-area section_padding_50_0">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-sm-5 col-lg-5">
                        <div className="single-footer-widget mb-100">
                            <NavLink to={"/"} className="mb-50 d-block"><img src={logo} alt=""/></NavLink>
                        </div>
                    </div>
                    <div className="col-12 col-sm-5 col-lg-5">
                        <div className="single-footer-widget mb-100">
                            <h5>Fast links</h5>
                            <ul>
                                <li><NavLink to={'/'} >Home </NavLink></li>
                                <li><NavLink to={'/login'} >Login/Register </NavLink></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-12 col-sm-2 col-lg-2">
                        <div className="single-footer-widget mb-100">
                            <h5>Contact Info</h5>
                            <div className="footer-single-contact-info d-flex">
                                <p>Endava</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer-bottom-area">
            <div className="container h-100">
                <div className="row h-100">
                    <div className="col-12 h-100">
                        <div
                            className="footer-bottom-content h-100 d-md-flex justify-content-between align-items-center">
                            <div className="copyright-text">
                                <p className="text-center">
                                    Copyright &copy;
                                    <script>document.write(new Date().getFullYear());</script>
                                    All rights reserved | This site is made with by X-Men
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;