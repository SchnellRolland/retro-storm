/**
 * Created by Rolli on 04-Nov-18.
 */

import {
    START_GET_RETRO_ITEMS,
    FINISH_GET_RETRO_ITEMS,
    CREATE_NEW_RETRO,
    ADD_RETRO_ROOM_ITEM
} from '../actions/actions';

const retroItemReducer = (state = {
    isSubmitting: false,
    response: {retroItems: []},
    error: false,
    retroRoomItems: {
        start: [],
        stop: [],
        keep: []
    }
}, action) => {

    switch (action.type) {
        case START_GET_RETRO_ITEMS:
            return {...state, isSubmitting: action.isSubmitting, error: action.error};
        case FINISH_GET_RETRO_ITEMS:
            return {...state, isSubmitting: action.isSubmitting, response: action.response, error: action.error};
        case CREATE_NEW_RETRO:
            return {...state, response: action.response, error: action.error};
        case ADD_RETRO_ROOM_ITEM: {
            return addRetroRoomItem(state, action.item)
        }
        default:
            return state;
    }
}

const addRetroRoomItem = (state, item) => {
    switch (item.type) {
        case "stop":
            return {...state, retroRoomItems: {stop: [...state.retroRoomItems.stop, item], start: [...state.retroRoomItems.start], keep: [...state.retroRoomItems.keep]}};
        case "start":
            return {...state, retroRoomItems: {start: [...state.retroRoomItems.start, item], stop: [...state.retroRoomItems.stop], keep: [...state.retroRoomItems.keep]}};
        case "keep":
            return {...state, retroRoomItems: {keep: [...state.retroRoomItems.keep, item], start: [...state.retroRoomItems.start], stop: [...state.retroRoomItems.stop]}};
        default:
            return state;
    }
};

export default retroItemReducer;