/**
 * Created by Rolli on 28-Oct-18.
 */

import {
    SUBMIT_USER_LOGIN,
    FINISH_USER_LOGIN,
    SUBMIT_USER_REGISTRATION,
    FINISH_USER_REGISTRATION,
    UPDATE_LOGIN_FORM
} from '../actions/actions'

const userReducer = (state = {
    isSubmitting: false,
    response: {},
    loginForm: {showPasswordMismatchMessage: false},
    error: false,
    loggedUsername: "",
    role: ""
}, action) => {


    switch (action.type) {
        case SUBMIT_USER_LOGIN:
            return {...state, isSubmitting: action.isSubmitting, loggedUsername: action.loggedUsername, role: action.role};
        case FINISH_USER_LOGIN:
            return {...state, isSubmitting: action.isSubmitting, response: action.response};
        case SUBMIT_USER_REGISTRATION:
            return {...state, isSubmitting: action.isSubmitting};
        case FINISH_USER_REGISTRATION:
            return {...state, isSubmitting: action.isSubmitting, response: action.response};
        case UPDATE_LOGIN_FORM:
            return {...state, loginForm: action.loginForm};

        default:
            return state;
    }
};

export default userReducer;