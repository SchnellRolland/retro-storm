import {combineReducers} from 'redux'
import userReducer from './user-reducer'
import retroItemReducer from './retro-item-reducer'

export default combineReducers({
    userReducer,
    retroItemReducer
})
