import fetch from 'cross-fetch'

export const SUBMIT_USER_LOGIN = "SUBMIT_USER_LOGIN";
export const FINISH_USER_LOGIN = "FINISH_USER_LOGIN";
export const SUBMIT_USER_REGISTRATION = "SUBMIT_NEW_USER";
export const FINISH_USER_REGISTRATION = "FINISH_USER_REGISTRATION";
export const UPDATE_LOGIN_FORM = "UPDATE_LOGIN_FORM";
export const START_GET_RETRO_ITEMS = "START_GET_RETRO_ITEMS";
export const FINISH_GET_RETRO_ITEMS = "FINISH_GET_RETRO_ITEMS";
export const CREATE_NEW_RETRO = "CREATE_NEW_RETRO";
export const ADD_RETRO_ROOM_ITEM = "ADD_RETRO_ROOM_ITEM";

export const submitUserLogin = userData => dispatch => {
    dispatch(startLoginSubmit());
    
    if (userData.username === "admin" && userData.password === "admin") {
        dispatch(finishLoginSubmit({status: "success", message: userData.username, loggedUsername: userData.username, role: "admin"}));
    } else {
        dispatch(finishLoginSubmit({status: "failed login", message: "Username / Password combination did not match", error: true}))
    }

    /*return fetch('https://reqres.in/api/users?page=1')
        .then(response => response.json())
        .then(json => dispatch(finishLoginSubmit(json)))*/
    
}

export const startLoginSubmit = () => ({
    type: SUBMIT_USER_LOGIN,
    isSubmitting: true
})

export const finishLoginSubmit = (response) => ({
    type: FINISH_USER_LOGIN,
    isSubmitting: false,
    response
})

const startSavingNewUser = () => ({
    type: SUBMIT_USER_REGISTRATION,
    isSubmitting: true
})

export const finishUserRegistration = (response) => ({
    type: FINISH_USER_REGISTRATION,
    isSubmitting: false,
    response
})

export const submitNewUser = newUserData => dispatch => {
    dispatch(startSavingNewUser());

    /*return fetch('https://reqres.in/api/users?page=1')
        .then(response => response.json())
        .then(json => dispatch(finishUserRegistration(json)))*/

    dispatch(finishUserRegistration({status: "success", message: "registration successful", error: false}));
}

export const updateLoginForm = (loginForm) => ({
    type: UPDATE_LOGIN_FORM,
    loginForm
})

export const startGetRetroItems = () => ({
    type: START_GET_RETRO_ITEMS,
    isSubmitting:true
})

export const getRetroItems = userId => dispatch => {
    dispatch(startGetRetroItems());
    
    let demoItems = [
        {id: "1", date: "20-12-2018", morale: 4.5, watsonValue: 0.24,
            retroDetails: {
                start: ["something to start 1", "something else to start 2"],
                stop: ["something to stop 1", "something else to stop2"],
                keep: ["something to keep 1", "something else to keep 2"]
            }},
        {id: "2", date: "22-05-2018", morale: 3, watsonValue: 0.8,
            retroDetails: {
                start: ["something to start 13", "something else to start 24"],
                stop: ["something to stop 15", "something else to stop 212"],
                keep: ["something to keep 133", "something else to keep 24"]
            }},
        {id: "3", date: "21-03-2018", morale: 4.2, watsonValue: 0.4, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "4", date: "11-03-2018", morale: 2.2, watsonValue: 0.2, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "5", date: "20-12-2018", morale: 4.5, watsonValue: 0.9, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "6", date: "22-05-2018", morale: 3, watsonValue: 0.5, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "7", date: "21-03-2018", morale: 4.2, watsonValue: 0.3, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "8", date: "11-03-2018", morale: 2.2, watsonValue: 0.7, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "9", date: "20-12-2018", morale: 4.5, watsonValue: 0.43, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "10", date: "22-05-2018", morale: 3, watsonValue: 0.2, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "11", date: "21-03-2018", morale: 4.2, watsonValue: 0.1, retroDetails: {start:[], stop:[], keep:[]}},
        {id: "12", date: "11-03-2018", morale: 2.2, watsonValue: 1, retroDetails: {start:[], stop:[], keep:[]}}
    ];
    
    dispatch(finishGetRetroItems({retroItems: demoItems}));
}

export const finishGetRetroItems = (response) => ({
    type: FINISH_GET_RETRO_ITEMS,
    isSubmitting: false,
    response: response    
})

export const createNewRetro = () => {
    /* get stuff */

    return {
        type: CREATE_NEW_RETRO,
        response: {},
        error: {}
    }
}

export const addRetroRoomItem = item => ({
        type: ADD_RETRO_ROOM_ITEM,
        item
})