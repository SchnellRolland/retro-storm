import { createStore, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';

import promiseMiddleware from 'redux-promise-middleware';

import  combineReducers  from '../reducers/app-reducer';

import logger from 'redux-logger'


const middleware = [promiseMiddleware(), logger, thunk ];

const initialState = {
    userReducer: {
        isSubmitting: false,
        error: false,
        loginForm: {
            loginUsername: "",
            loginPassword: "",
            registrationUsername: "",
            registrationPassword: "",
            registrationMatchPassword: "",
            registrationEmail: "",
            showPasswordMismatchMessage: false
        },
        response: {
            status: "",
            message: "",
            loggedUsername: "",
            role: ""
        }
    },
    retroItemReducer: {
        isSubmitting: false,
        error: false,
        retroName: "",
        retroLink: "",
        response: {
            retroItems: []
        },
        retroRoomItems: {
            start:[],
            stop:[],
            keep:[]
        }
    }
};

const AppStore = () => {
    return createStore(combineReducers, initialState, applyMiddleware(...middleware));
}
export default AppStore;