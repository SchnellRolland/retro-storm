// IMPORT PACKAGE REFERENCES

import React from 'react';
import { NavLink } from 'react-router-dom';

import logo from '../../resources/img/logo.png';

// COMPONENT

const Header = () => (
    <header className="header_area clearfix">
        <div className="container-fluid h-100">
            <div className="row h-100">
                <div className="col-12 h-100">
                    <div className="menu_area h-100">
                        <nav className="navbar h-100 navbar-expand-lg align-items-center">
                            <a className="navbar-brand" href="/"><img className="logo_image" src={logo} alt="Retro Storm" /></a>
                            <button className="navbar-toggler" type="button" data-toggle="collapse"
                                    data-target="#mosh-navbar" aria-controls="mosh-navbar" aria-expanded="false"
                                    aria-label="Toggle navigation"><span className="navbar-toggler-icon"></span>
                            </button>

                            <div className="collapse navbar-collapse justify-content-end" id="mosh-navbar">
                                <ul className="navbar-nav animated" id="nav">
                                    <li className="nav-item"><NavLink className="nav-link" to={"/"}>Home</NavLink></li>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" id="moshDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Site Actions</a>
                                        <div className="dropdown-menu header-dropdown" aria-labelledby="moshDropdown">
                                            <NavLink className="dropdown-item" to={"/myRetros"}> My retros </NavLink>
                                            <NavLink className="dropdown-item" to={"/createRetro"}> Create Retro </NavLink>
                                        </div>
                                    </li>
                                    <li className="nav-item"><NavLink className="nav-link" to={"/login"}>Login / Register</NavLink></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>


       </div>


    </header>
);

export default Header;