import sunny from "../resources/img/retro-item-images/sunny.png";
import sunnyAndClouds from "../resources/img/retro-item-images/clouds.png";
import rain from "../resources/img/retro-item-images/morning-rain.png";
import storm from "../resources/img/retro-item-images/storm.png";


const renderMorale = (morale) =>{
    if (morale > 0 && morale < 2.5) {
        return "red-morale"
    } else if (morale >= 2.5 && morale < 4) {
        return "yellow-morale"
    } else if (morale >=4) {
        return "green-morale"
    }
}

const renderImage = (watsonValue) => {
    if (watsonValue > 0 && watsonValue < 0.25) {
        return storm;
    } else if (watsonValue >= 0.25 && watsonValue < 0.5) {
        return rain;
    } else if (watsonValue >= 0.25 && watsonValue < 0.75) {
        return sunnyAndClouds;
    } else if (watsonValue >= 0.75) {
        return sunny;
    }
}

const randomUUID = () => {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export {renderMorale, renderImage, randomUUID};